CONTENTS OF THIS FILE
---------------------

 * About Disqus Inline

ABOUT DISQUS INLINE
-------------------

Comments are !important, they create a discussion between the author and 
the readers. Some comments are just simple feedback, some can help clarify 
a vague point, some comments can even offer a totally different approach for 
solving the same problem. The problem with most comments is context - 
in most cases, comment threads are positioned at the bottom of the post. 
In these situations, commenting on a specific point in the article becomes 
quite difficult, I guess this is why we have "quotes" - by repeating a 
certain part of the original post, you could refer to the specific point 
where it was mentioned and comment in that specific context. 
That was OK, for 2005, but what if you could simply comment anywhere?
